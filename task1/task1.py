ROWS_FILE_NAME = 'rows2.txt'

def read_file(file_name):
    with open(file_name, 'r') as file:
        lines = file.readlines()
        good_lines = list(map(lambda x: x.strip('\n').split(), lines))
        return good_lines


def format_line(line):
        int_line = list(map(lambda x: int(x), line))
        return int_line


def main(file_name):
    diffs = []
    good_lines = read_file(file_name)
    for line in good_lines:
        int_line = format_line(line)
        diff = max(int_line) - min(int_line)
        diffs.append(diff)
    control_sum = sum(diffs)
    return f'differences values: {diffs}, control sum: {control_sum}'


print(main(ROWS_FILE_NAME))

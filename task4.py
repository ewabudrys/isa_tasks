import random


def pkn():
    options = ['P', 'K', 'N']
    repeat = 't'
    user_win = 0
    computer_win = 0

    while repeat == 't':

        user_choice = input('Wybierz figure: (P)apier, (K)amień, (N)ożyce - ')
        user_choice = user_choice.upper()
        if user_choice not in options:
            raise ValueError('tylko P/K/N')

        print(f'Gracz: {user_choice}')
        computer_choice = random.choice(options)
        print(f'Komputer: {computer_choice}')

        if user_choice == computer_choice:
            print("Remis!")

        elif (user_choice == 'P' and computer_choice == 'K') or \
                (user_choice == 'K' and computer_choice == 'N') or \
                (user_choice == 'N' and computer_choice == 'P'):
            print("Gracz wygrywa!")
            user_win += 1

        else:
            print("Komputer wygrywa")
            computer_win += 1

        print(f"Aktualny wynik: Gracz {user_win} : {computer_win} Komputer")
        repeat = input('Jeśli chcesz kontunuować wybierz t: ')


print(pkn())

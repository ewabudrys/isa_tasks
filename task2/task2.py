import zipfile
import os
import pathlib


def unzip_a_file(file):
    dir = f"./{file.rstrip('.zip')}"
    if not os.path.exists(dir):
        os.makedirs(dir)

    with zipfile.ZipFile(file,"r") as zip_ref:
        zip_ref.extractall(dir)
    return dir

def read_file(file_name):
    with open(f'{file_name}', 'r') as file:
        word = file.readline()
        word = word.strip()
        formated_word = list(word)
        return formated_word


def count_ASCII(word):
    lower_word = [x.lower() for x in word]
    counter = dict((x,lower_word.count(x)) for x in set(lower_word))
    return counter


def main(file_name):
    dir = unzip_a_file(file_name)
    listofwords = []
    for file in os.listdir(dir):
        formated_word = read_file(file)
        for letter in formated_word:
            listofwords.append(letter)
    return count_ASCII(listofwords)


print(main('zadanie_1_words.zip'))


